import sys
sys.path.insert(0, '../tools/')
import os
os.environ['THEANO_FLAGS']="device=cuda,floatX=float32,lib.cnmem=0.7"
from keras.layers import Input, Convolution2D, Dense, Dropout
from keras.models import Model # basic class for specifying and training a neural network
from keras.callbacks import ModelCheckpoint # to save vals
import numpy as np
import load
import log


# ===========CONFIG FILES ===============
NETNAME = "net_saliency"
modelpath = "../models/"+NETNAME+".hdf5"


# ===========HYPER PARAM ================
img_shape = (400, 400) #size of input images
batch_size = 32 # in each iteration traing batch_size amount of images
num_epochs = 5000 # number of times to iterate over a training set
kernel_size_1 = 13 # kernel size for layer 1
kernel_size_2 = 5 # kernel size for layer 2
filters_1 = 16 # number of kernels for the first layer
filters_2 = 32 # number of kernels for the second layer
drop_prob_1 = 0.25 # dropout val 1
channels = 3 # channel size



#============ LOAD DATA ================



X, Y = load.get_data(channels, img_shape=img_shape)# retrieve (original, truth)
num_train, channels, height, width = X_train.shape # retrieving data set shape 


#============= MODEL ===================
inp = Input(shape=(channels, None, None)) # N.B. depth goes first in Keras!

conv_1 = Convolution2D(filters_1, kernel_size_1, kernel_size_1, border_mode='same', activation='relu')(inp)
conv_2 = Convolution2D(filters_2, kernel_size_2, kernel_size_2, border_mode='same', activation='relu')(conv_1)
drop_1 = Dropout(drop_prob_1)(conv_2)


conv_3 = Convolution2D(filters_1, kernel_size_2, kernel_size_2, border_mode='same', activation='relu')(drop_1)
conv_4 = Convolution2D(channels, kernel_size_1, kernel_size_1, border_mode='same', activation='relu')(conv_3)


model = Model(input=inp, output=conv_4) # To define a model, just specify its input and output layers


checkpointer = ModelCheckpoint(filepath=modelpath, verbose=0, save_best_only=False)

train_loss = []
validation_loss =[]
#============= MODEL RUN===============
model.compile(loss='mean_squared_error', # using the cross-entropy loss function
              optimizer='RMSprop', # using the Adam optimiser
              metrics=['accuracy']) # reporting the accuracy
for x in range(num_epochs):
	print("Epoch: "+ str(x))
	history = model.fit(X_train, Y_train, # Train the model using the training set...
	          batch_size=batch_size, nb_epoch=1,
	          verbose=0, validation_split=0.1, callbacks=[checkpointer]) # ...holding out 10% of the data for validation
	log.print_img(X_train[:5], model.predict(X_train[:5]),Y_train[:5], x, NETNAME)
	train_loss.append(history.history['loss'])
	validation_loss.append(history.history['val_loss'])
	log.loss_curve(train_loss,validation_loss)
