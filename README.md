# DeepCaustics
The source code for DeepCaustics as described in the _**IEEE JOE 2019 paper: DeepCaustics: Classification and Removal of Caustics From Underwater Imagery**_.

<pre>
@article{forbes2018deepcaustics,
 title={DeepCaustics: Classification and Removal of Caustics From Underwater Imagery},
 author={Forbes, Timothy and Goldsmith, Mark and Mudur, Sudhir and Poullis, Charalambos},
 journal={IEEE Journal of Oceanic Engineering},
 number={99},
 pages={1--11},
 year={2018},
 publisher={IEEE}
}
</pre>


